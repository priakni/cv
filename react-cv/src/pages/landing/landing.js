import './landing.scss';
import qrsvg from '../../assets/images/qr_code.svg';
import resumePdf from '../../assets/files/nikolai_priakhin.pdf';
import ExperienceElement from '../../components/experienceElement/experienceElement';
import ExperienceSeparator from '../../components/experienceSeparator/experienceSeparator';
import ContactsTableRow from '../../components/contactsTableRow/contactsTableRow';

const Landing = ({ colorScheme, toggleColorScheme }) => {
    return (
        <>
            <div className="landing_placeholder_small_screen">
                Nikolai Priakhin<br />software development lead<br /><br /><a href="tel:+79778048969">+7 977 804 89 69</a><br /><br /><a href="mailto:priakni@gmail.com">priakni@gmail.com</a>
            </div>
            <div className="landing">
                <div className="page_block color_scheme">
                    <p className="color_scheme__button" onClick={toggleColorScheme}>Go to {colorScheme === 'dark' ? 'light' : 'dark'} mode</p>
                </div>
                <div className="page_block title">
                    <div className="title__picture">
                        <img className="title__picture__img" src={require('../../assets/images/profile_picture.jpg')} alt="Nikolai Priakhin" />
                    </div>
                    <div className="title__name">
                        <h1>Nikolai Priakhin</h1>
                        <h2 className="title__name__subtitle">software development lead<span className="blink opacity_50">|</span></h2>
                    </div>
                </div>
                <div className="page_block contacts">
                    <div className="contacts__qr">
                        <img className="contacts__qr__img" src={qrsvg} alt="qr to this site" />
                    </div>
                    <h3 className="block_title contacts__title">Contact me</h3>
                    <table className="contacts__table">
                        <tbody>
                            <ContactsTableRow lineName="Email address" lineValue="priakni@gmail.com" lineLink="mailto:priakni@gmail.com"></ContactsTableRow>
                            <ContactsTableRow lineName="Phone number" lineValue="+7 977 804-89-69" lineLink="tel:+79778048969"></ContactsTableRow>
                            <ContactsTableRow lineName="Telegram" lineValue="@priakni" lineLink="https://t.me/priakni"></ContactsTableRow>
                            <ContactsTableRow lineName="LinkedIn" lineValue="priakni" lineLink="https://linkedin.com/in/priakni"></ContactsTableRow>
                            {/* <ContactsTableRow lineName="GitLab" lineValue="priakni" lineLink="https://gitlab.com/priakni"></ContactsTableRow> */}
                            <ContactsTableRow lineName="Resume" lineValue="download" lineLink={resumePdf} download="nikolai_priakhin.pdf"></ContactsTableRow>
                        </tbody>
                    </table>
                </div>
                <div className="page_block profile">
                    <h3 className="block_title profile__title">Profile</h3>
                    <div className="profile__content">
                        <p>Started coding in C at the age of 14. Over 6 years of .net c# development for ms dynamics crm. Experienced at angular + typescript.</p>
                        <p>Over two years of leadership and management with a team of 6 to 15 devs.</p>
                        <p>Multiple pet-projects and freelance in python + django</p>
                        <p>Fast autonomous problem solver with passion and care to quality of code.</p>
                    </div>
                </div>
                <div className="page_block portfolio">
                    <h3 className="block_title portfolio__title">Some works</h3>
                    <div className="portfolio__element">
                        <div className="portfolio__element__title"><a href="https://wishiro.com">Wishiro</a></div>
                        <div className="portfolio__element__status">Beta</div>
                        <div className="portfolio__element__description">Project for wishlist creation and maintenance</div>
                    </div>
                    <div className="portfolio__element">
                        <div className="portfolio__element__title"><a href="https://priakni.gitlab.io/tempuradev/">Tempura</a></div>
                        <div className="portfolio__element__status">Out of order</div>
                        <div className="portfolio__element__description">Web-design and development studio</div>
                    </div>
                    <div className="portfolio__element">
                        <div className="portfolio__element__title">Priakni</div>
                        <div className="portfolio__element__status">This site</div>
                        <div className="portfolio__element__description">CV website</div>
                    </div>
                </div>
                <div className="page_block experience">
                    <h3 className="block_title experience__title">Experience</h3>
                    <ExperienceElement titleName="Software Development Lead" companyName="Ingosstrakh" employmentPeriod="05.2022 – today" description="<p>I am a lead software developer at Ingosstrakh, a leading insurance company in Russia. My role involves overseeing development and managing a team to ensure successful project delivery. I work on upgrading the tech stack, which includes Dynamics CRM, C#, JavaScript, TypeScript, Angular to keep up with industry standards. I also manage two contractor companies and ensure their smooth operation. With a strong background in technology and leadership, I bring a wealth of expertise to this role.</p>"></ExperienceElement>
                    <ExperienceSeparator></ExperienceSeparator>
                    <ExperienceElement titleName="Software Development Lead" companyName="PIK" employmentPeriod="11.2019 – 05.2022" description="<p>Largest real estate development company in Russia.</p><p>Started as a developer for Dynamics CRM in .net + angular stack. Made different architectural decisions and suggested different solutions for system and team productivity improvements and became a development lead at 02.2020.</p><p>As a development lead I embedded CI/CD, development regulations and code review process. Implemented a process of developers employment and development planning.</p>"></ExperienceElement>
                    <ExperienceSeparator></ExperienceSeparator>
                    <ExperienceElement titleName="Software Developer" companyName="Société Générale" employmentPeriod="01.2018 – 11.2019" description="<p>Worked as a front-office systems developer for the #1 mortgage bank in Russia, initially as an outsourced specialist and later became a part of the core team. Developed a b2c mortgage system from a simple 'call me back' page to a fully autonomous system with document processing and credit approval.</p><p>In July 2019, the company became part of Rosbank, providing an opportunity to broaden experience and product development. Continued to work on the b2c mortgage system and initiated a new chat-bot platform for different bank products (mortgage, consumer loan, car loan, etc.)</p>"></ExperienceElement>
                    <ExperienceSeparator></ExperienceSeparator>
                    <ExperienceElement titleName="Software Developer" companyName="cmdsoft" employmentPeriod="10.2016 – 01.2018" description="<p>While studying at the university, I joined a company specializing in MS Dynamics CRM integration and support services. I developed a strong understanding of the product and honed my skills in creating efficient back-end and front-end solutions for enterprise clients. My work experience encompasses delivering a wide range of exceptional projects for banks, retail companies, and development firms.</p>"></ExperienceElement>
                </div>
            </div>
        </>
    );
};

export default Landing;
