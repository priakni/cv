import React, { useState } from 'react';
import './experienceElement.scss';

const ExperienceElement = ({ titleName, companyName, employmentPeriod, description }) => {
    const [isCollapsed, setIsCollapsed] = useState(false);

    return (
        <div className="experience__element">
            <div className="experience__element__title" onClick={() => setIsCollapsed(!isCollapsed)}>
                {isCollapsed ? '>' : '<'} {titleName}
            </div>
            <div className="experience__element__company" onClick={() => setIsCollapsed(!isCollapsed)}>@ {companyName}</div>
            <div className="experience__element__dates" onClick={() => setIsCollapsed(!isCollapsed)}>{employmentPeriod}</div>
            {
                !isCollapsed &&
                <div className="experience__element__description" dangerouslySetInnerHTML={{ __html: description }} />
            }
        </div>
    );
}

export default ExperienceElement;
