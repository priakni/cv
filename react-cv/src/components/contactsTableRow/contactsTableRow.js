import './contactsTableRow.scss';

const ContactsTableRow = ({ lineName, lineValue, lineLink, download }) => {
    return (
        <tr className="contacts__table__row">
            <td className="contacts__table__row__title">{lineName}: </td>
            <td className="contacts__table__row__value">
                {!!lineLink ? (
                    <a href={lineLink} download={download || undefined}>
                        {lineValue}
                    </a>
                ) : (
                    lineValue
                )}
            </td>
        </tr>
    );
};

export default ContactsTableRow;