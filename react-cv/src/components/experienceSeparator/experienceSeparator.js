import './experienceSeparator.scss';

const ExperienceSeparator = () => {
    return (
        <div className="experience__separator">==========================</div>
    )
};

export default ExperienceSeparator;