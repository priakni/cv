import { React, useEffect } from 'react';
import ReactDOM from 'react-dom/client';
// import { BrowserRouter, Routes, Route } from "react-router-dom";
import useColorScheme from './services/colorScheme';
import Landing from './pages/landing/landing';
import './index.scss';

export default function App() {
  const [colorScheme, toggleColorScheme] = useColorScheme();

  useEffect(() => {
    document.body.className = colorScheme;
  }, [colorScheme]);

  return (
    <Landing colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}/>
    // <BrowserRouter>
    //   <Routes>
    //   <Route exact path='/' component={() => <Landing colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}/>} />
    //   </Routes>
    // </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
