import { useState, useEffect } from 'react';

const useColorScheme = () => {
  const [colorScheme, setColorScheme] = useState(() => {
    const initialColorScheme = window.matchMedia('(prefers-color-scheme: dark)').matches
      ? 'dark'
      : 'light';
    return localStorage.getItem('colorScheme') || initialColorScheme;
  });

  useEffect(() => {
    const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
    const handleChange = (event) => {
      setColorScheme(event.matches ? 'dark' : 'light');
    };
    mediaQuery.addEventListener("change", handleChange);
    return () => mediaQuery.removeEventListener("change", handleChange);
  }, []);

  const toggleColorScheme = () => {
    if (colorScheme === 'dark' || colorScheme === 'light') {
      const newColorScheme = colorScheme === 'dark' ? 'light' : 'dark';
      localStorage.setItem('colorScheme', newColorScheme);
      setColorScheme(newColorScheme);
    } else {
      localStorage.removeItem('colorScheme');
      const initialColorScheme = window.matchMedia('(prefers-color-scheme: dark)').matches
        ? 'dark'
        : 'light';
      setColorScheme(initialColorScheme);
    }
  };

  return [colorScheme, toggleColorScheme];
};

export default useColorScheme;
